\documentclass{article}

\usepackage[margin=1.5cm]{geometry}
\usepackage{titlesec}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{algorithm2e}

\titleformat{\section}{\bfseries\large}{}{0cm}{}

\begin{document}

\begin{flushright}
    {\large \textbf{Harry Austen}}
\end{flushright}

\begin{center}
    {\huge \textbf{CM20252 Artificial Intelligence}}

    {\large \textbf{Revision}}
\end{center}

\section{Basics}
\begin{itemize}
    \item \textbf{Agent} - something that acts. A rational agent acts so as to achieve the best outcome or, when there is uncertainty, the best expected outcome
    \item \textbf{Completeness} - Is the algorithm guaranteed to find a solution when there is one?
    \item \textbf{Optimality} - Does it find the optimal solution?
    \item \textbf{Branching Factor} - Maximum number of successors of any node, $b$
    \item \textbf{Depth} - Depth of the shallowest goal node, $d$
    \item \textbf{Length} - Maximum length of any path, $m$
\end{itemize}

\section{Breadth First Search}
\begin{itemize}
    \item Uninformed (blind) search
    \item Expand the shallowest node on the frontier first
    \item \textbf{Complete?} Yes, if $d$ and $b$ are finite
    \item \textbf{Optimal?} Only if path cost is a nondecreasing function of node depth, for example, if all actions have identical cost
    \item \textbf{Time complexity} is $ 1 + b + b^2 + \hdots + b^d = O(b^d) $
    \item \textbf{Space complexity} is the maximum number of nodes in the frontier, which occurs when the frontier stretches across the entire bottom row of the tree, i.e. $O(b^d)$
    \item $O(b^d)$ is \textbf{SLOW}
\end{itemize}

\section{Depth First Search}
\begin{itemize}
    \item Uninformed (blind) search
    \item Expand the deepest node on the frontier first
    \item \textbf{Complete?} Yes, if graph search and finite state space. No, if tree search
    \item \textbf{Optimal?} No
    \item \textbf{Time complexity} is $O(b^m)$
    \item \textbf{Space complexity} is $O(bm)$
\end{itemize}

\section{Iterative Deepening Search}
\begin{itemize}
    \item Run depth first search to a fixed depth $z$
    \item Start at $z=1$. If no solution, increment $z$ and rerun
    \item Duplicates work, but actually does not expand many more nodes than breadth first search (top level nodes visited $m+1$ times, row two nodes visited $m$ times etc.)
    \item \textbf{Russel \& Norvig 2010} - In general, iterative deepening search is the preferred uninformed search method when the state space is large and the depth of the solution is unknown
\end{itemize}

\section{Uniform Cost Search}
\begin{itemize}
    \item Expand node with lowest \textbf{path cost} ($ f(n) = g(n) $)
    \item \textbf{Complete?} Yes, unless there is a path with an infinite sequence of zero cost actions
    \item \textbf{Optimal?} Yes
    \item \textbf{Time/space complexity} is $O(b^{d+1})$ with identical action costs, or $O(b^{\lfloor C^* / \epsilon \rfloor + 1})$ otherwise, where $C^*$ is the cost of the optimal solution and $\epsilon$ is the minimum action cost
\end{itemize}

\section{Summary}
\begin{tabular}{l|cccccc}
    & Breadth First & Uniform Cost & Depth First & Depth Limited & Iterative Deepening & Bidirectional \\ \hline
    Complete? & Yes$^a$ & Yes$^{a,b}$ & No & No & Yes$^a$ & Yes$^{a,d}$ \\
    Time & $O(b^d)$ & $O(b^{1+\lfloor C^* / \epsilon \rfloor})$ & $O(b^m)$ & $O(b^l)$ & $O(b^d)$ & $O(b^{d/2})$ \\
    Space & $O(b^d)$ & $O(b^{1+\lfloor C^* / \epsilon \rfloor})$ & $O(bm)$ & $O(bl)$ & $O(bd)$ & $O(b^{d/2})$ \\
    Optimal? & Yes$^c$ & Yes & No & No & Yes$^c$ & Yes$^{c,d}$
\end{tabular}

\vspace{.5cm}
\begin{center}
    \begin{tabular}{ll}
        $a$ & complete if $b$ finite \\
        $b$ & complete if step costs $ \ge \epsilon $ for positive $\epsilon$ \\
        $c$ & optimal if step costs are all identical \\
        $d$ & if both directions use breadth first search \\
    \end{tabular}
\end{center}

\section{Greedy Best-First Search}
\begin{itemize}
    \item Informed (heuristic) search
    \item Expand the node that is closest to the goal (lowest $ f(n) = h(n) $)
    \item \textbf{Complete?} Only when performing graph search in a finite state set. No otherwise (it can get into loops)
    \item \textbf{Optimal?} No
    \item \textbf{Time/space complexity} is $O(b^m)$ for tree search. But a good heuristic can reduce it substantially
\end{itemize}

\section{A$^*$ Search}
\begin{itemize}
    \item Informed (heuristic) search
    \item Expand the node that has the lowest $ f(n) = g(n) + h(n) $ (path cost + estimated cost to goal)
    \item \textbf{Complete?} Yes
    \item \textbf{Optimal?} Yes
        \begin{itemize}
            \item Tree search version is optimal when the heuristic is \textbf{admissable} - never overestimates cost to reach goal node (e.g. straight line distance)
            \item Graph search version is optimal when the heuristic is \textbf{consistent} - $ h(n) \le c(n,a,n') + h(n') $

                (Note: consistent $\implies$ admissable)
            \item A$^*$ is optimally efficient for any given consistent heuristic. No other optimal algorithm is guaranteed to expand fewer nodes than A$^*$
        \end{itemize}
    \item \textbf{Time/space complexity} - For most problems, the number of states A$^*$ visits is still exponential in the length of the solution. It usually runs out of space long before it runs out of time
\end{itemize}

\section{Heuristics}
\begin{itemize}
    \item There is often no single \textbf{best} heuristic, therefore we can use
        $$ h(n) = \max \{ h_1(n), \hdots , h_m(n) \} $$
        where $ h_1 \hdots h_m $ are all admissable heuristics. Then $h$ is admissable and dominates all of its component heuristics
\end{itemize}

\section{Local Search Algorithms}
\begin{itemize}
    \item Not systematic
    \item Use very little memory - usually constant
    \item Often find reasonable solutions in large state spaces for which systematic algorithms are unsuitable
\end{itemize}

\section{Hill Climbing Search}
\begin{itemize}
    \item Generate successors of current solution. Pick the best successor. Repeat until no neighbour has a better value than the current solution
    \item \textbf{Russel \& Norvig} - Trying to find the top of Mount Everest in a thick fog while suffering from amnesia
    \item \textbf{Random-Restart} - A variant of hill-climbing that searches randomly generated initial states until a goal is found
    \item Hill climbing is \textbf{incomplete} and random-restart is \textbf{inefficient}
\end{itemize}

\section{Simulated Annealing}
\begin{itemize}
    \item Generate successors of current solution. Consider a random successor. If it improves the situation, accept the move. Otherwise, accept it with some probability $p<1$
\end{itemize}

\section{Local Beam Search}
\begin{itemize}
    \item Keeping just one state in memory is rather extreme
    \item Keep track of $k$ states instead of 1
    \item Start with $k$ randomly generated states. Generate all successors of all $k$ states and select the $k$ best successors of the complete list
    \item \textbf{Problem} - Quite often all $k$ states end up on the same local hill
    \item \textbf{Stochastic Beam Search} introduces randomness by choosing successors randomly, biased towards good ones
\end{itemize}

\section{Adversarial Search}
\begin{itemize}
    \item Multiple agents with conflicting goals, known as a \textbf{game}
    \item As the same set of state space objects that a search problem has, called the game tree: Initial state, actions and a transition model
    \item Additionally has a terminal test, utility (payoff/objective function) and the notion of players, i.e. who's turn is it?
\end{itemize}

\section{Minimax}
\begin{itemize}
    \item Choose action with the highest minimax value to receive best possible payoff against best play
        $$ \text{minimax} =
        \begin{cases}
            \text{utility}(s) &\text{if $s$ is a terminal state} \\
            \max_{a \in \text{actions}(s)} \text{minimax}(\text{result}(s,a)) &\text{if player($s$) = player 1} \\
            \min_{a \in \text{actions}(s)} \text{minimax}(\text{result}(s,a)) &\text{if player($s$) = player 2}
        \end{cases}
        $$
    \item \textbf{Complete?} Yes, if game tree is finite
    \item \textbf{Optimal?} Yes, against an optimal opponent
    \item \textbf{Time complexity} is $O(b^m)$, where $m$ is the maximum depth of the tree and $b$ is the branching factor
    \item \textbf{Space complexity} is $O(bm)$ or $O(m)$ depending on the implementation
\end{itemize}

\section{Alpha-Beta Pruning}
\begin{itemize}
    \item $\alpha$ is the value of the best choice found so far for \texttt{MAX}
    \item $\beta$ is the value of the best choice found so far for \texttt{MIN}
    \item Pruning does not impact the solution but reduces time complexity from $O(b^m)$ to $O(b^{m/2})$
\end{itemize}

\section{Constraint Satisfaction}
\begin{itemize}
    \item $ X = \{ X_1, \hdots, X_n \} $ is a set of variables
    \item $ D = \{ D_1, \hdots, D_n \} $ is a set of domains specifying the values each variable can take
    \item $C$ is a set of constraints that specifies the values that the variables are allowed to have collectively
    \item \textbf{Arc Consistency}
        \begin{itemize}
            \item Maintain a set of all arcs
            \item Pop an arbitrary arc $(i,j)$ from the set
            \item Make the domain of node $i$ consistent with the domain of node $j$
            \item If domain of $i$ changes, add all arcs $(k,i)$ to the set
        \end{itemize}
    \item \textbf{Backtracking Search}
        \begin{itemize}
            \item Depth First Search
            \item Choose values for one variable at a time
            \item Backtrack when a variable has no legal values remaining
        \end{itemize}
\end{itemize}

\section{Probability}
\begin{itemize}
    \item \textbf{Unconditional} probability (\textbf{prior}) is a probability value in the absence of other information
    \item \textbf{Conditional} probability (\textbf{posterior}) is a probability value in the presence of additional information (\textbf{evidence})
        $$ P(a|b) = \frac{P(a \land b)}{P(b)} $$
    \item \textbf{Product rule}
        $$ P(a \land b) = P(a|b)P(b) $$
    \item Random variables defined with an uppercase letter, e.g. $Weather$
    \item Domain of a random variable is the set of possible values the variable can take, and is defined with a lowercase letter, e.g. $\{ sunny, rain, cloudy, snow\}$
    \item A \textbf{probability distribution} is the set of probabilities of each variable taking any of the values in the domain. The \textbf{full joint probability distribution} is the joint distribution for all the random variables and is normally represented by a table
    \item \textbf{Expected value}
        $$ E(X) = \sum_x xP(x) $$
    \item \textbf{Conditioning}
        $$ P(a) = \sum_z P(a|z)P(z) $$
    \item \textbf{Chain rule}
        \begin{align*}
            P(a,b,c) &= P(a|b,c)P(b|c)P(c) \\
            \implies P\left(\bigcap_{i=1}^n x_i\right) &= \prod_{j=1}^n P\left(x_j\left|\bigcap_{k>j} x_k\right.\right)
        \end{align*}
    \item \textbf{Bayes rule}
        $$ P(a|b) = \frac{P(b|a)P(a)}{P(b)} $$
    \item \textbf{Propositions} are independent if
        \begin{align*}
            P(a|b) &= P(a) \\
            \Leftrightarrow P(b|a) &= P(b) \\
            \Leftrightarrow P(a \land b) &= P(a)P(b)
        \end{align*}
        \textbf{Variables} are independent if
        \begin{align*}
            \bm{P}(X|Y) &= \bm{P}(X) \\
            \Leftrightarrow \bm{P}(Y|X) &= \bm{P}(Y) \\
            \Leftrightarrow \bm{P}(X,Y) &= \bm{P}(X)\bm{P}(Y)
        \end{align*}
\end{itemize}

\section{Markov Decision Process}
\begin{itemize}
    \item Method of sequential decision making under uncertainty
    \item State $s_{t+1}$ and reward $r_{t+1}$ depend probabilistically on state $s_t$ and action $a_t$
    \item Ideally, a state should summarise past sensations so as to retain all \textbf{essential} information, i.e. it should have the Markov Property (the future depends only on the present)
    \item Objective is to maximise the reward received in the \textbf{long term}
    \item The most commonly used metric is \textbf{expected discount return}
        \begin{align*}
            G_t &= r_{t+1} + \gamma r_{t+2} + \hdots \\
            &= \sum_k \gamma^k r_{t+k+1}
        \end{align*}
        where $\gamma$ is the \textbf{discount rate} ($ 0 \le \gamma \le 1 $). Values close to zero are called \textbf{shortsighted} and values close to one are called \textbf{farsighted}
    \item A finite MDP is defined by
        \begin{itemize}
            \item A set of \textbf{states} $S$
            \item A set of \textbf{actions} available from state $s$, $A(s)$, $ s \in S $
            \item One-step dynamics defined by \textbf{transition probabilities} $P(s'|s,a)$
            \item \textbf{Expected reward} $R(s',s,a)$
            \item An initial distribution
        \end{itemize}
    \item A solution of an MDP is a \textbf{policy} $\pi(s,a)$ that specifies the probability of taking action $a$ in state $s$
        \begin{itemize}
            \item Policies can be \textbf{deterministic} or \textbf{stochastic}
            \item An \textbf{optimal} solution is a policy that maximises the objective function (e.g. the expected discounted return)
            \item We can use \textbf{value functions} to organise the search for the optimal policy
        \end{itemize}
    \item \textbf{State Value Function} gives the expected return when starting in state $s$ and following policy $\pi$ thereafter
        \begin{align*}
            V^\pi(s) &= E_\pi \{ R_t | s_t = s \} \\
            &= E_\pi \left\{ \left. \sum_k \gamma^k r_{t+k+1} \right| s_t = s \right\}
        \end{align*}
    \item A policy $\pi$ is better than or equal to policy $\pi '$ if and only if
        $$ V^\pi(s) \ge V^\pi(s) \quad \forall s \in S $$
        There is always at least one policy that is better than or equal to all the others, called the \textbf{optimal policy}, denoted $\pi^*$. All the optimal policies share the same optimal state-value function $V^*$
    \item \textbf{Bellman Equation}
        $$ V^\pi(s) = \sum_a \pi(s,a) \sum_{s'} P_{ss'}^a [ R_{ss'}^a + \gamma V^\pi(s') ] $$
    \item \textbf{Bellman Optimality Equation} - The value of a state under an optimal policy must equal the expected return for the best action from that state
        $$ V^*(s) = \max_{a \in A(s)} \sum_{s'} P_{ss'}^a [ R_{ss'}^a + \gamma V^*(s') ] $$
    \item Using this as an iterative update equation, we receive the \textbf{Value Iteration Update}
        $$ V_{k+1}(s) = \max_{a \in A(s)} \sum_{s'} P_{ss'}^a [ R_{ss'}^a + \gamma V_k(s') ] $$
\end{itemize}

\section{Value Iteration}
\begin{itemize}
    \item The following algorithm converges to an optimal policy for discounted finite MDPs

        (The policy may converge long before the values do)
\end{itemize}
\begin{algorithm*}[H]
    Initialise array $v$ arbitrarily (e.g. $v(s)=0$ for all $s \in S^+$) \\
    \While{$\Delta < \theta$}
    {
        $ \Delta = 0 $ \\
        \ForEach{$s \in S$}
        {
            $ \text{temp} = v(s) $ \\
            $ v(s) = \max_a \sum_{s'} p(s'|s,a) [ r(s,a,s') + \gamma v(s') ] $ \\
            $ \Delta = \max ( \Delta, \lvert \text{temp} - v(s) \rvert ) $ \\
        }
    }
\end{algorithm*}

\section{Best Options}
What are the best algorithms to use to solve Tic-Tac-Toe
\begin{itemize}
    \item Against an \textbf{optimal opponent}?

        Adversarial search - \textbf{minimax} and alpha-beta pruning
    \item Against a suboptimal but \textbf{known opponent}?

        Model it as an MDP and apply \textbf{value iteration}
    \item Against an \textbf{unknown opponent}?

        \textbf{Reinforcment learning}
\end{itemize}

\section{Machine Learning}
\begin{itemize}
    \item An agent is \textbf{learning} if it improves its performance after making observations about the world
    \item \textbf{Supervised Learning} is the process of learning from examples
    \item \textbf{Reinforcement Learning} is the process of learning from interaction
\end{itemize}

\section{Reinforcement Learning}
\begin{itemize}
    \item A computational approach to \textbf{goal-directed} learning from iteration/trial and error
    \item The environment is typically stochastic and uncertain
    \item The agent acts on its environment and observes the \textbf{immediate} consequences
    \item The agent's goal is to obtain as much reward as possible in the long term
    \item Ideally, a state should summarise past sensations so as to retain all \textbf{essential} information (i.e. possess the Markov Property)
    \item If a reinforcement learning problem has the Markov Property, then it is a Markov Decision Process (MDP). If state and action sets are finite, it is a finite MDP
    \item The goal of an agent is specified in terms of a numerical reward signal. The objective is to maximise reward in the \textbf{long term}
    \item Performed in \textbf{episodic tasks}
        $$ G_t = r_{t+1} + r_{t+2} + \hdots + r_T $$
        or \textbf{continuing tasks}
        \begin{align*}
            G_t &= r_{t+1} + \gamma r_{t+2} + \hdots \\
            &= \sum_k \gamma^k r_{t+k+1}
        \end{align*}
\end{itemize}

\section{Q-Learning}
\begin{algorithm}[H]
    Initialise $Q(s,a)=k$ for all $s \in S$, $a \in A(s)$ \\
    \ForEach{\normalfont{epsiode}}
    {
        Initialise $s$ \\
        \ForEach{\normalfont{step $\in$ epsiode}}
        {
            Choose action $a$ from Q-derived policy (e.g. $\epsilon$-greedy) \\
            $ Q(s,a) += \alpha [ r + \gamma\max_{a'} Q(s',a') - Q(s,a) ] $ \\
            $ s = s' $ \\
        }
    }
\end{algorithm}

\section{Supervised Learning}
\begin{itemize}
    \item Classifier is trained on a \textbf{training data set} of $N$ example input-output pairs $(x_1,y_1), \hdots, (x_N,y_N)$, where each $y_i$ was generated by an unknown function $y=f(x)$, generating a function $h$ that approximates the true function $f$. The function $h$ (called the \textbf{hypothesis}) can be used to determine the expected classes of a set of \textbf{test data}
    \item We say that a hypothesis \textbf{generalises} well if it correctly predicts the value of $y$ for novel examples
    \item When the output $y$ is one of a finite set of values, the learning problem is called \textbf{classification} and when $y$ is a number, the learning problem is called a \textbf{regression}
    \item A simple example is when the class can be determined by its position on a graph of its properties with respect to the \textbf{decision boundary}
    \item k-nearest neighbour prediction of the class label of $\bm{x}$ (a vector of features)
        \begin{itemize}
            \item Find the $\bm{k}$ examples that are nearest to $\bm{x}$
            \item Take the majority vote
        \end{itemize}
        Problems
        \begin{itemize}
            \item How to define \textbf{nearest}? (e.g. Euclidean distance)
            \item How should we choose $k$?
            \item Normalisation recommended
        \end{itemize}
    \item \textbf{Ockham's Razor} - Prefer the simplest hypothesis consistent with the data
    \item The \textbf{error rate} is the proportion of inaccurate predictions in unseen instances
\end{itemize}

\section{Naive Bayes Classifier}
\begin{itemize}
    \item Assume the features are conditionally independent of each other, given the class
        $$ P(C|x_1, \hdots, x_k) = \alpha P(C) \prod_i P(x_i|C) $$
    \item \textbf{Laplace smoothing} - To avoid zero probabilities, we add one to each count
\end{itemize}

\section{Decision Trees}
\begin{itemize}
    \item Asking a set of questions about the attributes/features, arrive at an approximation for the class label
    \item Each branch of the tree is given a value
        $$ \text{entropy} = - \frac{p}{p+n} \log_2 \left( \frac{p}{p+n} \right) - \frac{n}{p+n} \log_2 \left( \frac{n}{p+n} \right) $$
        The entropy is a value between zero and one of how evenly split the number of positive and negative class labels are, e.g.
        \begin{equation*}
            \text{entropy} =
            \begin{cases}
                0 &p=0 \\
                1 &p=n \\
                0 &n=0
            \end{cases}
        \end{equation*}
\end{itemize}

\section{Logistic Regression}
\begin{itemize}
    \item The process of fitting the weights of a model, $\bm{w}$
    \item \textbf{Gradient descent} can be used to find values of $w$ that minimise the error in the training data
    \item The output of the model is a number between zero and one. It can be interpreted as the estimate probability of belonging to class 1
\end{itemize}

\section{Artificial Neural Networks}
\begin{itemize}
    \item An artificial neural network takes all features as arguments and outputs the sum of their weighted values
        \begin{equation*}
            \text{output} =
            \begin{cases}
                0 &\text{if } \sum_i w_ix_i \le t \\
                1 &\text{if } \sum_i w_ix_i > t
            \end{cases}
        \end{equation*}
        for some \textbf{threshold} value $t$
    \item \textbf{Perception} - the network \textbf{fires} when a linear combination of its inputs exceed some threshold
    \item Adding in a \textbf{bias weight} $w_0=t$, we retrieve
        \begin{equation*}
            \text{output} =
            \begin{cases}
                0 &\text{if } \sum_i w_ix_i \le 0 \\
                1 &\text{if } \sum_i w_ix_i > 0
            \end{cases}
        \end{equation*}
\end{itemize}

\section{Logic}
\begin{itemize}
    \item \textbf{Syntax} defines how to form sentences
    \item \textbf{Semantics} defines the truth of each sentence with respect to each possible world/model
    \item \textbf{Entailment} - when a sentence follows logically from others (\textbf{implication})
        $$ \alpha \models \beta $$
        Sentence $\alpha$ entails sentence $\beta$ if and only if, in every model in which $\alpha$ is true, $\beta$ is also true
        $$ \text{e.g. } ``x=0" \models ``xy=0" $$
    \item \textbf{Model checking} enumerates all possible models to check that $\beta$ is true in all models in which $\alpha$ is true
    \item \textbf{Logical Inference}
        $$ \alpha \vdash_i \beta $$
        denotes that sentence $\beta$ can be \textbf{derived from} $\alpha$ by inference algorithm $i$
    \item \textbf{Soundness} - Algorithm $i$ is sound if
        $$ ``\alpha \vdash_i \beta" \implies ``\alpha \models \beta" $$
    \item \textbf{Completeness} - Algorithm $i$ is complete is
        $$ ``\alpha \models \beta" \implies ``\alpha \vdash_i \beta" $$
\end{itemize}

\section{Syntax}
\begin{itemize}
    \item \textbf{Atomic sentences} are single propostition symbols ($P,Q,R,S_1,North,\hdots$) that evaluate to \textbf{true} or \textbf{false}
    \item \textbf{Complex sentences} are made using \textbf{logical connectives}:
        \begin{center}
            \begin{tabular}{ll}
                $\neg$ & not \\
                $\land$ & and \\
                $\lor$ & or \\
                $\Rightarrow$ & implies \\
                $\Leftrightarrow$ & iff
            \end{tabular}
        \end{center}
    \item If $S,S_1,S_2$ are sentences, then so are
        \begin{align*}
            &\neg S \\
            S_1 &\land S_2 \\
            S_1 &\lor S_2 \\
            S_1 &\Rightarrow S_2 \\
            S_1 &\Leftrightarrow S_2
        \end{align*}
\end{itemize}

\section{Semantics}
\begin{itemize}
    \item \textbf{Propositional logic} - A model assigns a truth value (\textbf{true} or \textbf{false}) for every proposition symbol
    \item Example: Given proposition \textbf{symbols} $P$ and $Q$, one possible \textbf{model} is $ m = \{ P = \textbf{true}, Q = \textbf{false} \} $
    \item Inference by enumeration (model checking)
        \begin{itemize}
            \item Enumerate the models - check that $\beta$ is true in every model in which $\alpha$ is true
            \item Depth-first enumeration of all models is \textbf{sound} and \textbf{complete} ($n$ symbols $\implies$ $2^n$ models), therefore, it has time complexity $O(2^n)$ and space complexity $O(n)$
        \end{itemize}
\end{itemize}

\section{Logical Equivalence}
\begin{itemize}
    \item Two sentences $\alpha$ and $\beta$ are logically equivalent ($\alpha\equiv\beta$) if they are true in the same set of models
    \item $\land$ and $\lor$ are \textbf{commutative}
        \begin{align*}
            \alpha \land \beta &\equiv \beta \land \alpha \\
            \alpha \lor  \beta &\equiv \beta \lor  \alpha
        \end{align*}
    \item $\land$ and $\lor$ are \textbf{associative}
        \begin{align*}
            (\alpha \land \beta) \land \gamma &\equiv \alpha \land (\beta \land \gamma) \\
            (\alpha \lor  \beta) \lor  \gamma &\equiv \alpha \lor  (\beta \lor  \gamma) \\
        \end{align*}
    \item \textbf{Double negation} elimination
        $$ \neg(\neg\alpha) \equiv \alpha $$
    \item \textbf{Contraposition}
        $$ \alpha \Rightarrow \beta \equiv \neg\beta \Rightarrow \neg\alpha $$
    \item \textbf{Implication} elimination
        $$ \alpha \Rightarrow \beta \equiv \neg\alpha \lor \beta $$
    \item \textbf{Biconditional} elimination
        $$ \alpha \Leftrightarrow \beta \equiv (\alpha \Rightarrow \beta) \land (\beta \Rightarrow \alpha) $$
    \item \textbf{De Morgan}
        \begin{align*}
            \neg(\alpha \land \beta) &\equiv \neg\alpha \lor  \neg\beta \\
            \neg(\alpha \lor  \beta) &\equiv \neg\alpha \land \neg\alpha
        \end{align*}
    \item \textbf{Distributivity}
        \begin{align*}
            \alpha \land (\beta \lor  \gamma) &\equiv (\alpha \land \beta) \lor  (\alpha \land \gamma) \\
            \alpha \lor  (\beta \land \gamma) &\equiv (\alpha \lor  \beta) \land (\alpha \lor  \gamma)
        \end{align*}
\end{itemize}

\section{Satisfiability}
\begin{itemize}
    \item A sentence is \textbf{satisfiable} if it is true in \textbf{some} model
        $$ A \lor B $$
    \item A sentence is \textbf{unsatisfiable} if it is true in \textbf{no} model
        $$ A \land \neg A $$
    \item $ \alpha \models \beta $ if and only if the sentence $ \alpha \land \neg\beta $ is unsatisfiable
\end{itemize}

\section{Propositional Logic}
\begin{itemize}
    \item An \textbf{interpretation} assigns a truth value to each symbol, e.g. $ \{ P = \text{true}, Q = \text{false} \} $
    \item A \textbf{meaning} is a set of interpretations
    \item The set of interpretations that is consistent with the knowledge base KB are \textbf{models} of KB
    \item It is \textbf{declarative}. Knowledge and inference are separate - inference is entirely independent of the domain
    \item It is \textbf{compositional}. Meaning of a sentence is a function of the meaning of its components. For example, the meaning of $ A \land B $ is derived from the meaning of $A$ and of $B$
    \item But, propsitional logic \textbf{lacks} the expressive power to describe words \textbf{concisely}
\end{itemize}

\section{Forward Chaining Algorithm}
\begin{itemize}
    \item Determines if a single propositional symbol $q$ is entailed by a knowledge base of definite clauses
    \item It begins with known facts. If all premises of an implication are known, it adds its conclusion to the set of known facts
    \item It stops when $q$ is added or when no further inferences can be made
    \item It is \textbf{sound} and \textbf{complete}
    \item Its \textbf{time complexity} is \textbf{linear} in the size of the knowledge base
\end{itemize}

\end{document}
